/*
 * structure is defined as of now
 * but some encryption/decryption values are incorrect
 *
 * 1. it's possible that array copying done using std::copy is messing up
 * 2. it's possible that reinterpret_cast is messing up
 *
 * */

#include <iostream>
#include <sodium.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include "cryptopp/files.h"
#include <sys/stat.h>
#include <uv.h>
#include <fstream>
//#include <spdlog/spdlog.h>

#define MSG (const unsigned char *) "Alexander Grahambell"
#define MSG_LEN 20
#define CIPHERTXT_LEN (crypto_kx_PUBLICKEYBYTES+crypto_box_MACBYTES)

using namespace std;
using namespace CryptoPP;

char * to_hex(char hex[], const uint8_t bin[], size_t length){
    uint8_t *p0 = (uint8_t *)bin;
    char *p1 = hex;
    for(int i=0;i<length;i++){
        snprintf(p1,3,"%02x",*p0);
        p0 += 1;
        p1 += 2;
    }
    return hex;
}

unsigned char peer1e_puk[crypto_kx_PUBLICKEYBYTES], peer1e_pk[crypto_kx_SECRETKEYBYTES];

unsigned char peer1_rx[crypto_kx_SESSIONKEYBYTES], peer1_tx[crypto_kx_SESSIONKEYBYTES];

unsigned char peer1_puk[crypto_box_PUBLICKEYBYTES], peer1_pk[crypto_box_SECRETKEYBYTES];

unsigned char peer2_puk[crypto_box_PUBLICKEYBYTES], peer2_pk[crypto_box_SECRETKEYBYTES];

unsigned char peer2_rx[crypto_kx_SESSIONKEYBYTES], peer2_tx[crypto_kx_SESSIONKEYBYTES];

unsigned char peer2e_puk[crypto_kx_PUBLICKEYBYTES], peer2e_pk[crypto_kx_SECRETKEYBYTES];

unsigned char rmt1e_puk[crypto_kx_PUBLICKEYBYTES], rmt2e_puk[crypto_kx_PUBLICKEYBYTES];

unsigned char nonce[crypto_box_NONCEBYTES];
unsigned char CIPHERTXT[CIPHERTXT_LEN];
unsigned char CIPHERTXT2[CIPHERTXT_LEN];

inline bool file_exists (const std::string& name) {
  struct stat buffer;
  return (stat (name.c_str(), &buffer) == 0);
}

void on_send(uv_udp_send_t *req, int status){
    if(status){
        fprintf(stderr, "Send Error %s\n", uv_strerror(status));
        return;
    }
    cout<<"on_send hit"<<endl;
    return;
}

void alloc_buffer(uv_handle_t *handle, size_t suggested_size, uv_buf_t *buf){
    buf->base=new char[suggested_size];
    buf->len=suggested_size;
}

void on_read(uv_udp_t *req, ssize_t nread, const uv_buf_t *buf, const struct sockaddr *addr, unsigned flags){
    
    if(nread<0){
        fprintf(stderr, "Read Error %s\n", uv_err_name(nread));
        uv_close((uv_handle_t*)req, NULL);
        free(buf->base);
        return;
    }

    cout<<"on_read hit"<<endl;

    unsigned char n[crypto_box_NONCEBYTES];
    int e = crypto_box_open_easy(rmt2e_puk,reinterpret_cast<unsigned char *>(buf->base),buf->len,n,peer1_puk,peer2_pk);
    if(e!=0){
        uv_close((uv_handle_t *)req,NULL);
    }
    char rmt_buf[CIPHERTXT_LEN+256];
    cout<<to_hex(rmt_buf,rmt2e_puk,CIPHERTXT_LEN)<<endl;
    return;
}

void on_read2(uv_udp_t *req, ssize_t nread, const uv_buf_t *buf, const struct sockaddr *addr, unsigned flags){
    
    if(nread<0){
        fprintf(stderr, "Read Error %s\n", uv_err_name(nread));
        uv_close((uv_handle_t *)req,NULL);
        free(buf->base);
        return;
    }

    cout<<"on_read2 hit"<<endl;
    
    unsigned char n[crypto_box_NONCEBYTES];
    int e = crypto_box_open_easy(rmt1e_puk,reinterpret_cast<unsigned char *>(buf->base),buf->len,n,peer2_pubk,peer1_pk);
    if(e!=0){
        uv_close((uv_handle_t *)req,NULL);
    }
    char rmt_buf[CIPHERTXT_LEN+256];
    cout<<to_hex(rmt_buf,rmt1e_puk,CIPHERTXT_LEN)<<endl;
    return;
}

uv_buf_t make_discover_msg(char cipher[], int n){
    uv_buf_t buffer;
    alloc_buffer(NULL, CIPHERTXT_LEN, &buffer);
    memset(buffer.base, 0, buffer.len);
    copy(cipher, cipher+n,buffer.base);
    return buffer;
}

int main(){
    /*peer 1 ephemeral and static key generation*/
    int e = crypto_kx_keypair(peer1e_puk, peer1e_pk);
    if(e != 0){
        cout<<"Failed Ephemeral Key generation"<<endl;
    }
    if (file_exists("static_pk.key") && file_exists("static_puk.key")){
        //load key
        cout<<"Loading existing Keys"<<endl;
        /*
        LoadPublicKey("static_puk.key",peer1_puk);
        LoadPrivateKey("static_pk.key",peer1_pk);
        */
    }else{
        cout<<"Generating and Saving static keys"<<endl;
        e = crypto_box_keypair(peer1_puk, peer1_pk);
        if(e != 0){
            cout<<"Failed Static Key Generation"<<endl;
            /*
            SavePrivateKey("static_pk.key",peer1_pk);
            SavePublicKey("static_puk.key",peer1_puk);
            */
        }
    }
    /*peer 2 ephemeral and static key generation*/
    e = crypto_kx_keypair(peer2e_puk, peer2e_pk);
    if(e != 0){
        cout<<"Failed Ephemeral Key generation"<<endl;
    }
    if (file_exists("static_pk.key") && file_exists("static_puk.key")){
        //load key
        cout<<"Loading existing Keys"<<endl;
        /*
        LoadPublicKey("static_puk.key",peer1_puk);
        LoadPrivateKey("static_pk.key",peer1_pk);
        */
    }else{
        cout<<"Generating and Saving static keys"<<endl;
        e = crypto_box_keypair(peer2_puk, peer2_pk);
        if(e != 0){
            cout<<"Failed Static Key Generation"<<endl;
            /*
            SavePrivateKey("static_pk.key",peer1_pk);
            SavePublicKey("static_puk.key",peer1_puk);
            */
        }
    }

    char epuhexbuf[crypto_kx_PUBLICKEYBYTES+256];
    char ephexbuf[crypto_kx_SECRETKEYBYTES+256];
    char spuhexbuf[crypto_box_PUBLICKEYBYTES+256];
    char sphexbuf[crypto_box_SECRETKEYBYTES+256];
    cout<<"Peer1: "<<endl;
    cout<<"Ephemeral Public Key: "<<to_hex(epuhexbuf, peer1e_puk, crypto_kx_PUBLICKEYBYTES)<<endl;
    cout<<"Ephemeral Private Key: "<<to_hex(ephexbuf, peer1e_pk, crypto_kx_SECRETKEYBYTES)<<endl;
    cout<<"Static Public Key: "<<to_hex(spuhexbuf, peer1_puk, crypto_box_PUBLICKEYBYTES)<<endl;
    cout<<"Static Private Key: "<<to_hex(sphexbuf, peer1_pk, crypto_box_SECRETKEYBYTES)<<endl;
    cout<<"+++++++++++++++++++++++++++++++++++++++++++++++"<<endl;
    cout<<"Peer2:"<<endl;
    cout<<"Ephemeral Public Key: "<<to_hex(epuhexbuf, peer2e_puk, crypto_kx_PUBLICKEYBYTES)<<endl;
    cout<<"Ephemeral Private Key: "<<to_hex(ephexbuf, peer2e_pk, crypto_kx_SECRETKEYBYTES)<<endl;
    cout<<"Static Public Key: "<<to_hex(spuhexbuf, peer2_puk, crypto_box_PUBLICKEYBYTES)<<endl;
    cout<<"Static Private Key: "<<to_hex(sphexbuf, peer2_pk, crypto_box_SECRETKEYBYTES)<<endl;

    /*encrypting ephemeral public key of peer1 with static public key of peer2
     *we authenticate this message with peer1 private key
     * */
    e = crypto_box_easy(CIPHERTXT, peer1e_puk, CIPHERTXT_LEN, nonce, peer2_puk, peer1_pk);
    if(e != 0){
        cout<<"Static Public Key encryption Failed"<<endl;
    }
    char cipherbuf[crypto_box_MACBYTES+crypto_kx_PUBLICKEYBYTES+256];
    cout<<to_hex(cipherbuf,CIPHERTXT,CIPHERTXT_LEN)<<endl;
    /*creating an uv udp send socket*/
    uv_loop_t *loop;
    loop = uv_default_loop();
    uv_udp_t send_socket;
    uv_udp_t recv_socket;
    uv_udp_init(loop,&send_socket);
    uv_udp_init(loop,&recv_socket);
    /*initializing & assigning ipv4 addresses. linking them to initialized sockets*/
    struct sockaddr_in send_addr;
    struct sockaddr_in recv_addr;
    uv_ip4_addr("127.0.0.1", 8043, &send_addr);
    uv_ip4_addr("127.0.0.1", 8021, &recv_addr);
    uv_udp_bind(&send_socket, (const struct sockaddr *)&send_addr, UV_UDP_REUSEADDR);
    uv_udp_bind(&recv_socket, (const struct sockaddr *)&recv_addr, UV_UDP_REUSEADDR);
    /*initializing buffer for receiving*/
    uv_buf_t msg=make_discover_msg(reinterpret_cast<char *>(CIPHERTXT), CIPHERTXT_LEN);
    uv_udp_send_t send_req;
    /*peer2 receiving messages encrypted using it's static pub key*/
    uv_udp_recv_start(&recv_socket, alloc_buffer, on_read);
    /*peer1 sending its ephemeral key*/
    uv_udp_send(&send_req, &send_socket, &msg, 1, (const struct sockaddr *)&recv_addr, on_send);
    /*peer2 sending its ephemeral key*/
    e = crypto_box_easy(CIPHERTXT2, peer2e_puk, CIPHERTXT_LEN, nonce, peer1_puk, peer2_pk);
    if(e != 0){
        cout<<"Peer2 Static Public Key Encryption Failed"<<endl;
    }
    cout<<to_hex(cipherbuf,CIPHERTXT2,CIPHERTXT_LEN)<<endl;
    msg = make_discover_msg(reinterpret_cast<char *>CIPHERTXT2, CIPHERTXT_LEN);
    uv_udp_send(&send_req, &recv_socket, &msg, 1, (const struct sockaddr *)&send_addr, on_send);
    /*peer1 recving messages encrypted using it's static private key*/
    uv_udp_recv_start(&send_socket, alloc_buffer,on_read2);
    /***********Both have others Ephemeral Public Key********************/
    /*Peer1 Key Generation*/
    crypto_kx_client_session_keys(peer1_rx, peer1_tx, peer1e_puk, peer1e_pk, rmt2e_puk);
    unsigned char xbuf[crypto_kx_SESSIONKEYBYTES+256];
    cout<<to_hex(xbuf, peer1_rx, crypto_kx_SESSIONKEYBYTES)<<endl;
    cout<<to_hex(xbuf, peer1_tx, crypto_kx_SESSIONKEYBYTES)<<endl;
    /*Peer2 Key Generation*/
    crypto_kx_server_session_keys(peer2_rx, peer2_tx, peer2e_puk, peer2e_pk, rmt1e_puk);
    cout<<to_hex(xbuf,peer2_rx, crypto_kx_SESSIONKEYBYTES)<<enndl;
    cout<<to_hex(xbuf,peer2_tx, crypto_KX_SESSIONKEYBYTES)<<endl;
    /*AES Encryption*/
    
    return uv_run(loop,UV_RUN_DEFAULT);
}
